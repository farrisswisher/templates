use askama::Template;
use warp::reply::Html;
use warp::{Filter, Rejection, Reply};

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {}

#[derive(Template)]
#[template(path = "hello.html")]
struct HelloTemplate<'a> {
    name: &'a str,
}

pub fn get_routes() -> impl Filter<Extract = (impl Reply,), Error = Rejection> + Clone {
    let index = warp::path::end().map(index_to_string);

    let hello = warp::path("hello").map(hello_to_string);

    let assets = warp::path("assets").and(warp::fs::dir("assets"));

    index
        .or(hello)
        .or(assets)
}

fn index_to_string() -> Html<String> {
    let page = IndexTemplate {};
    warp::reply::html(page.render().expect("error"))
}

fn hello_to_string() -> Html<String> {
    let page = HelloTemplate { name: "World" };
    warp::reply::html(page.render().expect("error"))
}

