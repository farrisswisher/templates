{
  description = "Farris Swisher's development templates";

  outputs = { self, nixpkgs }: {

    templates = {
        rust-htmx-hello = {
            path = ./rust-htmx-hello;
            description = "Basic Hello World with Rust and HTMX";
        };
    };

  };
}
